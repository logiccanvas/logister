var production = false;

/*** Required
******************************/

var gulp = require('gulp'),
	gulpif = require('gulp-if'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	jade = require('gulp-jade'),
	plumber = require('gulp-plumber'),
	autoprefixer =	require('gulp-autoprefixer'),
	del =	require('del'),
	prettify = require('gulp-prettify');



/*** Scripts
******************************/
gulp.task('scripts', function(){
	gulp
		.src('gulp/src/js/**/*.js')
		.pipe(plumber())
		.pipe( gulpif(production, uglify() ) )
		.pipe( gulpif(production, rename({suffix:'.min'})) )
		.pipe(gulp.dest('app/js'));
});


/*** Styles
******************************/
gulp.task('styles', function(){
	gulp
		.src('gulp/src/css/**/*.scss')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe( autoprefixer( 'last 2 versions' ) )
		.pipe(gulp.dest('app/css'));
});


/*** HTML
******************************/
gulp.task('htdocs', function(){ 

	var YOUR_LOCALS = {};
	gulp
		.src(['gulp/src/html/**/*.jade', '!gulp/src/html/layout/**', '!gulp/src/html/includes/**'])
		.pipe(plumber())
		.pipe(
			jade({ locals: YOUR_LOCALS })
		)
		.pipe( prettify({indent_size: 4}) )
		.pipe(gulp.dest('app/'));
});


/*** Fonts
******************************/
gulp.task('fonts', function(){ 
	gulp
		.src('gulp/src/fonts/**/*.*')
		.pipe(gulp.dest('app/fonts/'));
});


// Images
gulp.task('images', function (){
    gulp
    	.src('gulp/src/images/**/*')
        .pipe(gulp.dest('app/images'));
});

// Build Clear
gulp.task('build:clear', function (){
	del([
		'build/**'
	])
});


// Build Copy
gulp.task('build:copy', function (){
	return gulp
    	.src('app/**/*/')
    	.pipe(gulp.dest('build/'));
});


/*** Watch
******************************/
gulp.task('watch', function(){
	gulp.watch('gulp/src/html/**/*.jade', ['htdocs']);
	gulp.watch('gulp/src/js/**/*.js', ['scripts']);
	gulp.watch('gulp/src/css/**/*.scss', ['styles']);
	gulp.watch('gulp/src/fonts/**/*.*', ['fonts']);
	gulp.watch('gulp/src/images/**/*', ['images']);

});

/*** Default
******************************/
gulp.task('default', ['htdocs', 'scripts', 'styles', 'fonts', 'images']);


