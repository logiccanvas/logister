
    // var app = angular.module('app', ['ngFileUpload']);
    app.controller('HomeController', HomeController);
    app.controller('LoginController', LoginController);
    app.controller('RegisterController', RegisterController);


    HomeController.$inject = ['UserService', '$rootScope'];
    function HomeController(UserService, $rootScope) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;

        initController();

        function initController() {
            loadCurrentUser();
            loadAllUsers();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
    }

    
    LoginController.$inject = ['$scope', '$location', 'AuthenticationService', 'FlashService'];
    function LoginController($scope, $location, AuthenticationService, FlashService) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/');
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };


        $scope.remember = false;

        $scope.rememberMe = function() {
            if ( $scope.remember ) {
                AuthenticationService.SetCredentials(vm.username, vm.password);
            } else {
                AuthenticationService.ClearCredentials();
            }

        }
    }


    RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService', '$scope', 'Upload', '$timeout'];
    function RegisterController(UserService, $location, $rootScope, FlashService, $scope, Upload, $timeout) {
        var vm = this;
        vm.register = register;


        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });
        $scope.$watch('file', function () {
            if ($scope.file != null) {
                $scope.files = [$scope.file]; 
            }
        });
        $scope.log = '';
        $scope.filename = '';

        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  if (!file.$error) {
                    Upload.upload({
                        url: '',
                        data: {
                          file: file  
                        }
                    }).then(function (resp) {
                        $timeout(function() {
                            $scope.log = 'file: ' +
                            resp.config.data.file.name +
                            ', Response: ' + JSON.stringify(resp.data) +
                            '\n' + $scope.log;
                        });
                    }, null, function (evt) {
                        var progressPercentage = parseInt(100.0 *
                                evt.loaded / evt.total);
                        $scope.filename = file.name;
                        $scope.log = 'progress: ' + progressPercentage + 
                            '% ' + evt.config.data.file.name + '\n' + 
                          $scope.log;
                    });
                  }
                }
            }
        };

        function register() {
            vm.dataLoading = true;
            vm.user.filename = $scope.filename;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response.success) {
                        console.log(vm.user.filename);
                        FlashService.Success('Registration successful', true);
                        $location.path('/');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }



    }
